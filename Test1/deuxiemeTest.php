<?php

namespace Test1;

require 'ClasseDeTest.php';


use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testComputeTVAOtherProduct()
    {
        $product = new Product('Un autre produit', 'Un autre type de produit', 20);

        $this->assertSame(3.92, $product->computeTVA());
    }
}
